# Low-Code Taipy를 사용하여 데이터 프로젝트를 위한 웹 앱 구축

## Taipy로 시작하기
Taipy로 만든 사용자 인터페이스는 전적으로 Python으로 구동된다.

![](./images/1_5vN75gnKzVFZios7qCad8w.webp)

개발자가 Taipy를 사용하여 웹 어플리케이션을 만들 때 알아야 할 점은 단 두 가지이다.

- **마크다운 형식의 문자열**: 여기에는 웹 앱에 표시하고자 하는 정보가 포함된다. 위에서 언급했듯이 Taipy는 웹 앱에 시각적 요소를 추가할 수 있는 증강 마크다운(augmented markdown) 형식을 제공한다. 텍스트 필드, 버튼, 슬라이더, 파일 선택기 또는 차트를 포함할 수 있다. 모든 것을 마크다운 문자열 안에 지정한다.

    ![](./images/0_t4io_EVbjaW-1-nI.webp)

- **Taipy GUI 객체**: 다음으로, 마크다운 문자열의 요소를 렌더링하여 웹 앱에 표시하는 GUI 객체가 있다.

    ![](./images/1_RpLYzDO-CCe5UCCCB9nO7A.webp)

여기까지. Taipy로 웹 앱을 설정하려면 몇 가지 간단한 단계가 필요하다.

### Taipy 설치
먼저 다음과 같이 Taipy를 설치한다.

```bash
$ pip install taipy
```

이제 간단한 마크다운 형식을 사용하여 웹 앱을 만들 수 있다. 

## Taipy로 간단한 웹 앱 개발
위에서 언급했듯이 Taipy를 사용하여 만든 웹 앱은 주로 마크다운 문자열과 GUI 객체로 구동된다.

### 1. 라이브러리 가져오기
웹 어플리케이션을 빌드하기 위해 먼저 Taipy 라이브러리를 임포트하여야 한다. 좀 더 구체적으로, 다음과 같이 `Gui` 객체를 임포트한다.

```python
## my_taipy_app.py

from taipy import Gui
```

### 2. 시각적 요소로 마크다운 정의하기
다음으로 웹 앱에 간단한 마크다운을 추가해 보겠다.

```python
## my_taipy_app.py

page = """
# First Taipy Web Application.
"""
```

여기서는 `page` 문자열 객체를 정의한다. `#`로 시작하는 문장은 마크다운 형식에 따른 큰 제목을 나타낸다.

### 3. Gui 객체를 정의하고 웹 앱 실행하기
마지막 단계는 아래 그림과 같이 마크다운을 `Gui` 객체에 전달하고 웹 앱을 실행하는 것이다.

```python
## my_taipy_app.py

Gui(page=page).run()
```

이름에서 알 수 있듯이 `run()` 메서드는 스크립트가 실행될 때 어플리케이션을 실행하기 위해 Gui 객체에서 호출된다.

> **Note**: *Taipy는 Jupyter Notebook에서도 사용할 수 있다.*

이것으로 웹 앱이 준비되었다.

### 4. 스크립트 실행
마지막으로 다른 Python 프로그램을 실행할 때와 마찬가지로 스크립트를 실행한다.

```bash
$ python my_taipy_app.py
```

이렇게 하면 로컬 서버가 실행되고 브라우저에서 웹 어플리케이션이 실행된다.

![](./images/1_9GKGGm0azWmGnQK_5CEAZA.webp)

## 시각적 요소 추가하기
다음으로 마크다운에 시각적 요소를 더 추가하여 웹 앱에 표시하는 방법을 살펴보자.

Taipy의 모든 시각적 요소는 특정 구문을 따른다.

![](./images/1_1A90dgc5cIlsbG_9lkcfdw.webp)

웹 앱에 시각적 요소를 추가하려면 마크다운 문자열 페이지 내에서 위에서 언급한 구문을 사용해야 한다.

아래에서 몇 가지 예를 살펴보겠다.

> **Note**: *여기서는 마크다운 문자열 객체 `page`만 수정하겠다. 나머지 코드는 동일하게 유지된다*.

### 1. 변수 디스플레이
웹 앱에서 변수를 디스플레이하려면 변수 이름을 정의한 다음 위에서 설명한 형식(예: `<|{name}|>`)으로 묶어 마크다운에 추가한다.

```python
# my_taipy_app.py

from taipy import Gui


page = """
# First Taipy Web Application!

Hello: <|{message}|text|>
"""

message = "이 윤준!"

if __name__ == "__main__":
    gui = Gui(page).run()
```

결과적으로 웹 앱에 다음과 같은 출력이 디스플레이된다.

![](./images/ScreenShot_01.png)

변수는 정수(또는 실수)일 수도 있다.

```python
## my_taipy_app.py

from taipy import Gui

page = """
# First Taipy Web Application.

Variable value: <|{variable}|>
"""

variable = 100

if __name__ == "__main__":
    gui = Gui(page).run()
```

결과적으로 웹 앱에는 다음과 같은 내용이 디스플레이된다.

![](./images/1_mdL2SB943tyHglNIc7KU6g.webp)

### 2. 슬라이더
다음으로, 변수를 슬라이더로 정의하려면 시각적 요소 구문을 따르고 `visual_element_name`을 `slider`로 정의해야 한다.

```python
## my_taipy_app.py

from taipy import Gui

page = """
# First Taipy Web Application.

Variable value: <|{variable}|>

Change Value: 

<|{variable}|slider|>
"""

variable = 100

if __name__ == "__main__":
    gui = Gui(page).run()
```

여기서는 슬라이더 시각적 요소에서 먼저 슬라이더가 제어할 변수(`variable`)를 정의하고, 다음으로 `slider` 타입으로 정의한다.

> *Taipy의 VS 코드 확장자를 사용하면 마크다운 코딩을 간소화할 수 있다. 이 확장 프로그램은 코드 완성과 마크다운 미리 보기 기능을 제공하여 코딩을 쉽도록 해준다.*

결과적으로 Taipy는 웹 앱에 슬라이더를 디스플레이한다.

![](./images/1_hr_B_hMXI4s2sk_fKW3ANg.gif)

또한 슬라이더를 움직이면 변수 값도 업데이트된다. 이는 두 시각적 요소가 동일한 객체(`variable`)를 참조하기 때문이다.

![](./images/1_wUWXf6vFUDZ1fE2pcqEngw.webp)

### 3. 차트
다음으로 데이터 과학 애플리케이션에서 매우 중요한 차트를 추가하는 방법을 살펴보겠다.

시각적 요소의 형식은 아래와 같이 동일하게 유지된다.

![](./images/1_HohQZQY87NFWHc6hN2MxIQ.webp)

예를 들어 scatter 차트를 디스플레이하려면 다음 구문으로 정의한다.

![](./images/1_o2iKUwtd99SpaXbMiOtIlg.webp)

시각적 요소를 정의하는 표준 구문을 따른다. 먼저 `data` 객체가 있다. 그런 다음 시각적 요소 타입을 `chart`로 선언한다.

다음으로, 디스플레이 `mode`를 `marker`로 지정하고 `type` 매개변수를 사용하여 산점도(scatter plot)를 표시한다. 마지막으로, 차트의 축을 정의하기 위해 `x`와 `y` 정보를 지정한다.

![](./images/1_00SWOZwk3O7pYNwpUbck3w.gif)

Taipy 차트는 [Plotly](https://plotly.com/python/)를 기반으로 하며, 차트를 대화형으로 만들었다.

Taipy 차트에 대한 종합적인 가이드는 [이 문서](https://bit.ly/3pztJFp)에서 확인할 수 있다.

### 4. 데이터 테이블 디스플레이
마지막으로 웹 애플리케이션에서 데이터 프레임을 인쇄해 보겠다.

```python
## my_taipy_app.py

data = {"Rating":[1,4,3,1,3,5,2,4,2],
        "Salary":[100,450,340,120,290,600,250,430,220]}

page = """
# First Taipy Web Application.

DataFrame: <|{data}|table|>
"""
```

이렇게 하려면 시각적 요소 타입을 `table`로 지정하면 끝이다!

그 결과 웹 어플리케이션에서 데이터 프레임을 볼 수 있다.

![](./images/1_L81PvX7XgOluRbmh9vTpXA.webp)

이것은 Taipy의 테이블에 대한 기본적인 데모이지만, 앱 내에서 필터링, 편집, 집계, 삭제와 같은 훨씬 더 고급 작업을 수행할 수 있다.

공식 가이드 [Taipy Table](https://bit.ly/44LvvDi)에서 자세한 내용을 읽어보세요.

## 마치며
이것으로 이 포스팅의 마지막을 장식하려고 한다.

이번 포스팅에서는 Taipy GUI를 사용하여 로우코드로 우아한 웹 앱을 구축하는 방법을 설명하였다.

좀 더 구체적으로, Taipy의 증강 마크다운 형식도 다루었다. 텍스트, 슬라이더, 차트 등의 시각적 요소를 단 몇 줄의 코드로 어플리케이션에 임베드하는 방법에 대해 논의했다.

이 데모에서는 단일 페이지 웹 앱을 만들었다. 하지만 Taipy로 여러 페이지의 어플리케이션을 만들 수도 있다. [Taipy로 여러 페이지 어플리케이션 만들기](https://bit.ly/3DFTMhS) 가이드를 참조하세요.

이 글의 범위를 넘어서는 내용이지만, Taipy GUI는 대용량 데이터에 대한 그래픽 지원, 사전 정의된 CSS 템플릿, 코어를 위한 새로운 GUI 요소 등과 같은 많은 깔끔한 기능으로 가득 차 있다.

마지막으로, 이 포스팅에서는 Taipy를 사용해 GUI 어플리케이션을 구축하는 방법을 다루었지만, [Taipy를 이용한 대규모 데이터 파이프라인 구축](https://pythonnecosystems.gitlab.io/build-large-data-pipelines-using-taipy)에서 비즈니스 사용 사례에 맞는 고급 데이터 기반 어플리케이션을 만드는 방법도 배울 수 있다.
