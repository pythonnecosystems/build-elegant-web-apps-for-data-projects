# Low-Code Taipy를 사용하여 데이터 프로젝트를 위한 웹 앱 구축 <sup>[1](#footnote_1)</sup>

오늘날 점점 더 많은 기업이 데이터를 기반으로 중요한 의사결정을 내리고 있는 상황에서, 이러한 주요 의사결정의 원동력이 되는 인사이트를 효과적으로 전달하는 것은 매우 중요하다.

이 과정에서 기업은 이해 관계자가 정보에 입각한 의사결정을 내릴 수 있도록 대시보드/웹 앱을 만들어 데이터를 제시하는 등 효과적인 로우코드(low-code) 도구가 필요한 경우가 자주 발생한다.

하지만 이러한 대화형 데이터 기반 웹 어플리케이션을 구축하는 것은 복잡하고 시간이 많이 소요되는 과정이다. 이는 특히 데이터 분석에 주로 집중하는 데이터 팀에게 큰 어려움이 될 것이다.

![](./images/1_LR8Mz52PoDptD1QwjFqVOQ.webp)

저자가 개인적으로 경험한 기존 솔루션의 가장 큰 문제 중 하나는 코드 작성에 대한 의존도가 높다는 점이다.

인터랙티브하고 고급스러우며 우아한 데이터 기반 웹 앱을 만드는 데 전문 지식이 전혀 필요하지 않고 간단한 Python 코드 몇 줄만 있으면 된다면 얼마나 좋을까?

![](./images/1_KHELael8Gv7zAx_JyWIL4g.webp)

이를 위해 Taipy는 매우 적은 코드로 데이터 기반 웹 앱 제작을 간소화하는 오픈소스 도구이다.

따라서 이 포스팅에서는 Taipy, 특히 Taipy GUI를 활용하여 아주 적은 코드로 복잡한 인터랙티브 GUI를 만드는 방법을 보여주고자 한다.

이제 시작해 보자 🚀!

<a name="footnote_1">1</a>: [Build Elegant Web Apps for Your Data Projects Using Low-Code Taipy](https://towardsdev.com/build-elegant-web-apps-for-your-data-projects-using-low-code-taipy-1706d6974a37)를 편역한 것이다.
